const { When, Then } = require('cucumber');
const neow = require('../support/pages/inputPinjaman');

When('User set detail loan with {string} and partnered {string} and tenure {string}', async function (cab, prog, tenor){
    
    await neow.informasi.pilihinstitusi(cab, prog, tenor);
});

When('User set detail loan with {string} and non-partnered {string}', async function (cab, prog){
    
    await neow.informasi.pilihnonpartnerinstitusi(cab, prog);
});

Then('User can see tabel cicilan', async () => {
    await testController.expect(neow.getTabelCicilan()).contains('Detail Pinjaman Anda');
});

Then('User can click ajukan dana pinjaman', async () => {
    await neow.informasi.btnAjukanDana();
})