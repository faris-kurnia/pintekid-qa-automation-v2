## Cara menggunakan

Script ini menggunakan NodeJS pastikan sudah install NodeJS.

1. Clone **git clone https://faris-kurnia@bitbucket.org/faris-kurnia/pintekid-qa-automation-v2.git**.
2. Create **.env**. contoh **BASE_URL=https://neow.pintek.id/**.
3. Install **npm install**.
4. Run Test **npm run test** atau **npx cucumber-js**.