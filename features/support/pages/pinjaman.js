const { Selector } = require('testcafe');
require('dotenv').config();

exports.contentPinjam = {
    async pilihloan(){
        await testController
    .click('#btn-signup')
    },
    
    async setInstitusi(inst) {
        await testController

            .typeText(Selector('#search_institution'), inst )
            .wait(3000)
            .click(Selector('ul.typeahead.dropdown-menu > li > a.dropdown-item').withText(inst))
            //.click(Selector('ul.typeahead.dropdown-menu > li > a.dropdown-item').withText('Educate Youngs'))
            .click(Selector('button').withText('Cari Sekarang'));
    }
};

exports.getDetailLaon = function () {
    return Selector('ul.nav.nav-pills > li > a.pintek-black').with({ boundTestRun: testController }).textContent;
};