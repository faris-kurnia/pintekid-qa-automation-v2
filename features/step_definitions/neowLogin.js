const { Given, When } = require('cucumber');
const neow = require('../support/pages/neow');


Given('User can open pintek landing page', async () => {
    await neow.navigateToThisPage();
});

When('User click nav login', async function (){
    await neow.navMasuk();
});
