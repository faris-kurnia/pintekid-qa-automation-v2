const { When, Then } = require('cucumber');
const neow = require('../support/pages/neow');

When('User click nav About Us', async function (){
    await neow.navAboutUs();
});

Then('User can see About Us page', async function (){
    await testController.expect(neow.getAboutUsPage()).contains('Tentang Kami');
});

When('User click nav Activity', async function (){
    await neow.navActivity();
});

Then('User can see Activity page', async function (){
    await testController.expect(neow.getActivityPage()).contains('Sosialisasi');
});

When('User click nav Risk Disclaimer', async function (){
    await neow.navRiskDisclaimer();
});

Then('User can see Risk Disclaimer page', async function (){
    await testController.expect(neow.getRiskDisclaimerPage()).contains('Disclaimer Risiko');
});

When('User click nav Term and Conditions', async function (){
    await neow.navTnC();
});

Then('User can see Term and Conditions page', async function (){
    await testController.expect(neow.getTnCPage()).contains('Syarat & Ketentuan');
});

When('User click nav Privacy Policy', async function (){
    await neow.navPrivacyPolicy();
});

Then('User can see Privacy Policy page', async function (){
    await testController.expect(neow.getPrivacyPolicyPage()).contains('Kebijakan Privasi');
});

When('User click nav FAQ', async function (){
    await neow.navFaq();
});

Then('User can see FAQ page', async function (){
    await testController.expect(neow.getFaqPage()).contains('Pertanyaan yang sering ditanyakan');
});

When('User click nav Digital Signature Policy', async function (){
    await neow.navDigitalSignPolicy();
});

Then('User can see Digital Signature Policy page', async function (){
    await testController.expect(neow.getDigitalSignPolicyPage()).contains('Ketentuan Penggunaan Tanda Tangan Elektronik');
});

/* When('User click nav Instagram of Pintek', async function (){
    await neow.navIgPintek();
});

Then('User can see Instagram of Pintek page', async function (){
    await testController.expect(neow.getDigitalSignPolicyPage()).contains('Ketentuan Penggunaan Tanda Tangan Elektronik');
}); */