const { When, Then } = require('cucumber');
const neow = require('../support/pages/inputSMEloan');

When('User input all field in step 1 Invoice Financing', async function (){
    await neow.step1.detailPinjaman1();
});

When('User input all field in step 2 Informasi Peminjam', async function (){
    await neow.step1.informasiPeminjam();
});

When('User input all field in step 2 Profil Pemohon', async function (){
    await neow.step1.profilPemohon();
});

When('User input all field in step 2 Daftar Pemilik', async function (){
    await neow.step1.daftarPemilik();
});

When('User input all field in step 2 Daftar Manajemen', async function (){
    await neow.step1.daftarManajemen();
});

When('User input all field in step 2 Informasi Penjamin Perseorangan', async function (){
    await neow.step1.penjaminPerseorangan();
});

When('User input all field in step 2 Informasi Penjamin Badan Usaha', async function (){
    await neow.step1.penjaminBadanHukum();
});

When('User input all field in step 2 Dokumentasi Wajib', async function (){
    await neow.step1.DokumenWajib();
});

When('User input all field in step 2 Dokumentasi Pendukung', async function (){
    await neow.step1.DokumenPendukung();
});

When('User input all field in step 3 Informasi Tambahan', async function (){
    await neow.step1.informasiPeminjam();
});

When('User input all field in step 4 Detail Pencairan', async function (){
    await neow.step1.detailPencairan();
});

When('User input all field in step 1 Working Capital', async function (){
    await neow.step1.detailPinjaman2();
});

When('User submit data aplikasi', async function (){
    await neow.step1.SelesaiSubmit();
});