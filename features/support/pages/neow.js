const { Selector } = require('testcafe');
require('dotenv').config();

const url = `${process.env.BASE_URL}`;

exports.navigateToThisPage = async function () {
    return testController.navigateTo(url);
};

exports.navigateSukses = async function () {
    return testController
        .navigateTo('https://neow.pintek.id/dashboard')
        .wait(2000);
};

exports.getPageTitle = function () {
    return Selector('.col-lg-4.text-16.light-grey').with({ boundTestRun: testController }).textContent;
};

exports.navMasuk = async function () {
    await testController
        .click(Selector('a').withText('Masuk'));
};

exports.navDaftar = async function () {
    await testController
        .click(Selector('a').withText('Daftar'));
};

exports.contentLongin = {
    async inputEmailPass() {
        await testController

            .typeText(Selector('#login_email'), 'dew.stu@yopmail.com')
            .typeText(Selector('#login_password'),'123456')
            .pressKey('enter');
    }
};

exports.seePopUp = function () {
    return Selector('div.row.modal-contents.text-center span').with({ boundTestRun: testController }).textContent;
};

exports.getSelamatDatang = function () {
    return Selector('div.row.content-margin.text-30').with({ boundTestRun: testController }).textContent;
};

exports.navigateToForgotPswdPage = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id/forgot-password')
        .click(Selector('.text-16.pull-right.pintek-blue').withText('Lupa Kata Sandi?'))
        .wait(2000);
};

exports.contentForgotPswd = {
    async ResetPswd() {
        await testController
            .typeText(Selector('input[name="email"]'), 'dewicust02@yopmail.com')
            .pressKey('enter');
    }
}

exports.getSuccessResetPswd = function () {
    return Selector('#success_message').with({ boundTestRun: testController }).textContent;
};

exports.navAboutUs = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id/tentang-kami')
        .click(Selector('a.text-16').withText('Tentang Kami'))
        .wait(2000);
};

exports.getAboutUsPage = function () {
    return Selector('h2.text-center').with({ boundTestRun: testController }).textContent;
};

exports.navActivity = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id/aktivitas')
        .click(Selector('a.text-16').withText('Aktivitas'))
        .wait(2000);
};

exports.getActivityPage = function () {
    return Selector('p.text-30.text-center.pintek-black.helvetica').with({ boundTestRun: testController }).textContent;
};

exports.navRiskDisclaimer = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id/disclaimer-risiko')
        .click(Selector('a.text-16').withText('Disclaimer Resiko'))
        .wait(2000);
};

exports.getRiskDisclaimerPage = function () {
    return Selector('p.text-30.text-center.pintek-black.helvetica').with({ boundTestRun: testController }).textContent;
};

exports.navTnC = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id//terms-conditions')
        .click(Selector('a.text-16').withText('Syarat & Ketentuan'))
        .wait(2000);
};

exports.getTnCPage = function () {
    return Selector('p.text-20.text-center.pintek-black').with({ boundTestRun: testController }).textContent;
};

exports.navPrivacyPolicy = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id//privacy-policy')
        .click(Selector('a.text-16').withText('Kebijakan Privasi'))
        .wait(2000);
};

exports.getPrivacyPolicyPage = function () {
    return Selector('p.text-30.text-center.pintek-black.title').with({ boundTestRun: testController }).textContent;
};

exports.navFaq = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id//faq')
        .click(Selector('a.text-16').withText('FAQ'))
        .wait(2000);
};

exports.getFaqPage = function () {
    return Selector('p.text-36.pintek-black.title.text-center').with({ boundTestRun: testController }).textContent;
};

exports.navDigitalSignPolicy = async function () {
    return testController
        //.navigateTo('https://neow.pintek.id//ketentuan-tandatangan-digital')
        .click(Selector('a.text-16').withText('Ketentuan Tanda Tangan Digital'))
        .wait(2000);
};

exports.getDigitalSignPolicyPage = function () {
    return Selector('p.text-36.pintek-black.title.text-center').with({ boundTestRun: testController }).textContent;
};