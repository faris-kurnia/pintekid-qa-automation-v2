const { When, Then } = require('cucumber');
const neow = require('../support/pages/neow');

When('User click nav regist', async function (){
    await neow.navDaftar();
});

Then('User can see regist page', async () => {
    await testController.expect(neow.getPageTitle()).contains('Atau daftar dengan');
});