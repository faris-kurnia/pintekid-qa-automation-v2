const { When, Then } = require('cucumber');
const neow = require('../support/pages/pinjaman');

When('User pilih education loan', async function (){
    
    await neow.contentPinjam.pilihloan();
});

When('User set {string} and click cari sekarang', async function (inst){
    await neow.contentPinjam.setInstitusi(inst);
});

Then('User can see detail pinjaman', async () => {
    await testController.expect(neow.getDetailLaon()).contains('Detail Pinjaman')
    .wait(2000);
});

