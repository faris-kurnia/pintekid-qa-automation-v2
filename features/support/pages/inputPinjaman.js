const { Selector } = require('testcafe');
require('dotenv').config();
//const vardata = require('../mydata/institutiondata');

const institusiSelect = Selector('#institution_branches');
const institusiOption = institusiSelect.find('option');
//const institusiOption = institusiSelect.find('option[value="618"]');

const programSelect = Selector('#institution_programs');
const programOption = programSelect.find('option');
//const programOption = programSelect.find('option[value="2466"]');

const tenorSelect = Selector('#loan_tenor');
//const tenorOption = tenorSelect.find('option[value="6"]');
const tenorOption = tenorSelect.find('option');

const DPselect = Selector ('#down_payment')
const DPoption = DPselect.find('option[value="0.5"]')

/*exports.pinjam = {
    async informasi() {
        await testController
        .click(institusiSelect)
        .click(institusiOption.withText('EY Palembang'))
        .wait(3000)
        .click(programSelect)
        .click(programOption.withText('New EY Training'))
        .wait(3000)
        .click(tenorSelect)
        .click(tenorOption.withText('6 Bulan'))
        .wait(3000)
        .click(Selector('input[name="payment_to_insitution_stat"]'))
        .wait(2000)
        .click(Selector('input[name="learn_start_date_stat"]'))
        .wait(2000)
        .click(Selector('#btn_save').withText('Dapatkan Penawaran'))
        .wait(2000)

    },*/
    exports.informasi = {
        async pilihinstitusi(cab, prog, tenor){
            await testController
        .click(institusiSelect)
        .click(institusiOption.withText(cab))
        .wait(3000)
        .click(programSelect)
        .click(programOption.withText(prog))
        .wait(3000)
        .click(tenorSelect)
        .click(tenorOption.withText(tenor))
        .wait(3000)
        .click(Selector('input[name="payment_to_insitution_stat"]'))
        .wait(2000)
        .click(Selector('input[name="learn_start_date_stat"]'))
        .wait(2000)
        .click(Selector('#btn_save').withText('Dapatkan Penawaran'))
        .wait(2000)

    },
    async pilihnonpartnerinstitusi(cab, prog){
        await testController
    .click(institusiSelect)
    .click(institusiOption.withText(cab))
    .wait(3000)
    .click(programSelect)
    .click(programOption.withText(prog))
    .wait(3000)
    .click(tenorSelect)
    .click(tenorOption.withText('6 Bulan'))
    .wait(3000)
    .click(DPselect)
    .click(DPoption.withText(" 50% "))
    .wait(500)
    .click(Selector('input[name="payment_to_insitution_stat"]'))
    .wait(2000)
    .click(Selector('input[name="learn_start_date_stat"]'))
    .wait(2000)
    .typeText(Selector('#txt_program_fee'), "50000000")
    .click(Selector('#btn_save').withText('Dapatkan Penawaran'))
    .wait(2000)
},
    async btnAjukanDana (){
        await testController
        .click(Selector('#btn_submit_application').withText('Ajukan Dana Pinjaman'))
        .wait(2000)
    }
};

exports.getDetailLaon = function () {
    return Selector('ul.nav.nav-pills > li > a.pintek-black').with({ boundTestRun: testController }).textContent;
};

exports.getTabelCicilan = function () {
    return Selector('div.row > p.text-30.text-center').with({ boundTestRun: testController }).textContent;
};