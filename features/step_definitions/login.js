const { When, Then } = require('cucumber');
const neow = require('../support/pages/neow');

When('User can input email and password', async function (){
    await neow.contentLongin.inputEmailPass();
});

Then('User can see pop up sukses', async () => {
    await testController.expect(neow.seePopUp()).contains('Selamat!');
});

Then('User can click button Ok in pop up', async function (){
    await neow.navigateSukses();
});

Then('User can see Selamat Datang Pinters', async function (){
    await testController.expect(neow.getSelamatDatang()).contains('Selamat Datang Pinters!');
});

