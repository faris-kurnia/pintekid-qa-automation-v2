Feature: Apply Loan Full - Neow

  User can open neow and apply loan

Scenario Outline: User completes personal data <cabang> dan <program>
    Given User can open pintek landing page
    When User click nav login
      And User can input email and password
    Then User can see pop up sukses
    Then User can click button Ok in pop up
    When User pilih education loan
    When User set <institusi> and click cari sekarang
    Then User can see detail pinjaman
    When User set detail loan with <cabang> and partnered <program> and tenure <tenor>
    Then User can see tabel cicilan
      And User can click ajukan dana pinjaman
    When User input all field in step 1 which borrower not same as student
    Then User can see data keluarga dan rekan
    When User input all field in step 2
    When User input all field in step 3
    When User input all field in step 4
    When User submits the application
 
 Examples: 
    | institusi | cabang | program | tenor |
    | 'MRA Broadcasting Academy' | 'Sarinah' | 'Radio Broadcast' | '2 Bulan' |
    | 'MRA Broadcasting Academy' | 'Sarinah' | 'Radio Broadcast' | '2 Bulan' |
    | 'MRA Broadcasting Academy' | 'Sarinah' | 'Radio Broadcast' | '2 Bulan' |
    | 'MRA Broadcasting Academy' | 'Sarinah' | 'Radio Broadcast' | '2 Bulan' |
    | 'MRA Broadcasting Academy' | 'Sarinah' | 'Radio Broadcast' | '2 Bulan' |
    