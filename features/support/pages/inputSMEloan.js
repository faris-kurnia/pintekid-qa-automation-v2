const { Selector } = require('testcafe');
require('dotenv').config();

//const debug = ClientFunction(() => { debugger; });

//const loantypeSelect = Selector('select[name="loan_type"]');
const loantypeSelect = Selector('#loan_type');
const loantypeOption = loantypeSelect.find('option');

const provinsiSelect = Selector('#payor_province_select');
const provinsiOption = provinsiSelect.find('option[value="Jawa Tengah"]');

const kotaSelect = Selector('#payor_district');
const kotaOption = kotaSelect.find('option[value="Magelang"]');

const kecamatanSelect = Selector('#payor_subdistrict');
const kecamanatanOption = kecamatanSelect.find('option[value="Muntilan"]');

const kelurahanSelect = Selector('#payor_village');
const kelurahanOption = kelurahanSelect.find('option[value="16624"]');

const jenisusahabSelect = Selector('#borrower_c_business_type');
const jenisusahabOption = jenisusahabSelect.find('option[value="1"]');

const bidangusahabSelect = Selector('#borrower_c_sector_of_industry');
const bidangusahabOption = bidangusahabSelect.find('option[value="3"]');

const bcprovinsiSelect = Selector('#borrower_c_province_select');
const bcprovinsiOption = bcprovinsiSelect.find('option[value="DI Yogyakarta"]');

const bckotaSelect = Selector('#borrower_c_district');
const bckotaOption = bckotaSelect.find('option[value="Gunung Kidul"]');

const bckecamatanSelect = Selector('#borrower_c_sub_district');
const bckecamanatanOption = bckecamatanSelect.find('option[value="Purwosari"]');

const bckelurahanSelect = Selector('#borrower_c_village');
const bckelurahanOption = bckelurahanSelect.find('option[value="4224"]');

const mcprovinsiSelect = Selector('#borrower_c_mailing_province_select');
const mcprovinsiOption = mcprovinsiSelect.find('option[value="DKI Jakarta"]');

const mckotaSelect = Selector('#borrower_c_mailing_district');
const mckotaOption = mckotaSelect.find('option[value="Jakarta Pusat"]');

const mckecamatanSelect = Selector('#borrower_c_mailing_subdistrict');
const mckecamanatanOption = mckecamatanSelect.find('option[value="Gambir"]');

const mckelurahanSelect = Selector('#borrower_c_mailing_village');
const mckelurahanOption = mckelurahanSelect.find('option[value="4872"]');

const jabatanPICSelect = Selector('#borrower_pic_jobtitle');
const jabatanPICOption = jabatanPICSelect.find('option[value="3"]');

const picprovinsiSelect = Selector('#borrower_pic_mailing_province_select');
const picprovinsiOption = picprovinsiSelect.find('option[value="Bali"]');

const pickotaSelect = Selector('#borrower_pic_mailing_district');
const pickotaOption = pickotaSelect.find('option[value="Buleleng"]');

const pickecamatanSelect = Selector('#borrower_pic_mailing_subdistrict');
const pickecamanatanOption = pickecamatanSelect.find('option[value="Sukasada"]');

const pickelurahanSelect = Selector('#borrower_pic_mailing_village');
const pickelurahanOption = pickelurahanSelect.find('option[value="227"]');

const jabatanPemilikSelect = Selector('#owner_jobtitle_add');
const jabatanPemilikOption = jabatanPemilikSelect.find('option[value="3"]');

const jabatanManajemenSelect = Selector('#management_jobtitle_add');
const jabatanManajemenOption = jabatanManajemenSelect.find('option[value="2"]');

const guarantorprovinsiSelect = Selector('#guarantor_p_province_select');
const guarantorprovinsiOption = guarantorprovinsiSelect.find('option[value="Lampung"]');

const guarantorkotaSelect = Selector('#guarantor_p_district');
const guarantorkotaOption = guarantorkotaSelect.find('option[value="Lampung Selatan"]');

const guarantorkecamatanSelect = Selector('#guarantor_p_sub_district');
const guarantorkecamanatanOption = guarantorkecamatanSelect.find('option[value="Jati Agung"]');

const guarantorkelurahanSelect = Selector('#guarantor_p_village');
const guarantorkelurahanOption = guarantorkelurahanSelect.find('option[value="38339"]');

const guarantortypeSelect = Selector('#guarantor_type');
const guarantortypeOption = guarantortypeSelect.find('option');

const jenisusahagSelect = Selector('#guarantor_business_type');
const jenisusahagOption = jenisusahagSelect.find('option[value="2"]');

const guarantor2provinsiSelect = Selector('#guarantor_c_province_select');
const guarantor2provinsiOption = guarantor2provinsiSelect.find('option[value="Lampung"]');

const guarantor2kotaSelect = Selector('#guarantor_c_district');
const guarantor2kotaOption = guarantor2kotaSelect.find('option[value="Lampung Selatan"]');

const guarantor2kecamatanSelect = Selector('#guarantor_c_sub_district');
const guarantor2kecamanatanOption = guarantor2kecamatanSelect.find('option[value="Jati Agung"]');

const guarantor2kelurahanSelect = Selector('#guarantor_c_village');
const guarantor2kelurahanOption = guarantor2kelurahanSelect.find('option[value="38339"]');

const bidangusahaSelect = Selector('#sector_of_industry');
const bidangusahaOption = bidangusahaSelect.find('option[value="2"]');

const tipeRekSelect = Selector('#bank_account_type');
const tipeRekOption = tipeRekSelect.find('option[value="2"]');

const namaBankSelect = Selector('#bank_id');
const namaBankOption = namaBankSelect.find('option[value="20"]');

exports.step1 = {
    async detailPinjaman1(){
        await testController
        .click(loantypeSelect)
        .click(loantypeOption.withText('Pembayaran Tagihan atau Invoice Financing'))
        .typeText(Selector('#loan_purpose'),'Data Tujuan Meminjam Invoice Financing')
        .wait(100)
        .selectText(Selector('#loan_amount_fake'))
        .pressKey('delete')
        .typeText(Selector('#loan_amount_fake'),'500000000')
        .wait(100)
        .selectText(Selector('#duration-year'))
        .pressKey('delete')
        .typeText(Selector('#duration-year'), '1')
        .selectText(Selector('#duration-month'))
        .pressKey('delete')
        .typeText(Selector('#duration-month'), '6')
        .wait(100)
        .selectText(Selector('#invoice_amount_fake'))
        .pressKey('delete')
        .typeText(Selector('#invoice_amount_fake'),'1000000000')
        .wait(100)
        .click(Selector('.form-check-label').withText('Sejak'))
        .wait(100)
        .typeText(Selector('input[name="payor_since"]'), '2010-05-14')
        .wait(100)
        .typeText(Selector('#payor_phone'),'082212345678')
        .wait(100)
        .typeText(Selector('#payor_name'),'Nama Payor IF')
        .wait(100)
        .typeText(Selector('input[name="payor_pic"]'),'Nama PIC Payor IF')
        .wait(100)
        .typeText(Selector('#payor_address'),'Alamat Payor IF')
        .wait(100)
        .click(provinsiSelect)
        .click(provinsiOption.withText('Jawa Tengah'))
        .wait(200)
        .click(kotaSelect)
        .click(kotaOption.withText('Magelang'))
        .wait(200)
        .click(kecamatanSelect)
        .click(kecamanatanOption.withText('Muntilan'))
        .wait(200)
        .click(kelurahanSelect)
        .click(kelurahanOption.withText('Tamanagung'))
        .wait(200)
        .setFilesToUpload('#invoice_file', [
            __dirname+"/images/doc1.jpg"
        ])
        .click(Selector('#btn_tab1_next').withText('Selanjutnya'))
        .wait(200)
    },

    async detailPinjaman2(){
        await testController
        .click(loantypeSelect)
        .click(loantypeOption.withText('Modal Usaha atau Working Capital'))
        .typeText(Selector('#loan_purpose'),'Data Tujuan Meminjam Modal Usaha')
        .wait(100)
        .selectText(Selector('#loan_amount_fake'))
        .pressKey('delete')
        .typeText(Selector('#loan_amount_fake'),'500000000')
        .wait(100)
        .selectText(Selector('#duration-year'))
        .pressKey('delete')
        .typeText(Selector('#duration-year'), '1')
        .selectText(Selector('#duration-month'))
        .pressKey('delete')
        .typeText(Selector('#duration-month'), '6')
        .wait(100)
        .click(Selector('#btn_tab1_next').withText('Selanjutnya'))
        .wait(200)
    },

    async informasiPeminjam(){
        await testController
        .click(jenisusahabSelect)
        .click(jenisusahabOption.withText('PT'))
        .typeText(Selector('input[name="borrower_c_name"]'),'PT Peminjam Satu')
        .wait(100)
        .click(bidangusahabSelect)
        .click(bidangusahabOption.withText('Jasa Pendidikan'))
        .typeText(Selector('#borrower_c_address'),'Jalan Kemayoran Selatan No. 59A')
        .wait(100)
        .click(bcprovinsiSelect)
        .click(bcprovinsiOption.withText('DI Yogyakarta'))
        .wait(100)
        .click(bckotaSelect)
        .click(bckotaOption.withText('Gunung Kidul'))
        .wait(100)
        .click(bckecamatanSelect)
        .click(bckecamanatanOption.withText('Purwosari'))
        .wait(100)
        .click(bckelurahanSelect)
        .click(bckelurahanOption.withText('Giripurwo'))
        .wait(100)
        .typeText(Selector('#borrower_c_mailing_address'),'Jalan Selasih Raya No. 95B')
        .wait(100)
        .click(mcprovinsiSelect)
        .click(mcprovinsiOption.withText('DKI Jakarta'))
        .wait(100)
        .click(mckotaSelect)
        .click(mckotaOption.withText('Jakarta Pusat'))
        .wait(100)
        .click(mckecamatanSelect)
        .click(mckecamanatanOption.withText('Gambir'))
        .wait(100)
        .click(mckelurahanSelect)
        .click(mckelurahanOption.withText('Petojo Utara'))
        .wait(100)
        .typeText(Selector('#borrower_c_phone'),'02123456789')
        .wait(100)
        .typeText(Selector('#borrower_c_tax_id'),'555444333222111')
        .wait(100)
        .setFilesToUpload('#borrower_c_tax_id_file', [
            __dirname+"/images/1.png"
        ])
        .wait(200)
        .setFilesToUpload('#borrower_c_deed_of_establishment', [
            __dirname+"/images/2.png"
        ])
        .wait(200)
        .setFilesToUpload('#borrower_c_last_change_deed', [
            __dirname+"/images/3.png"
        ])
        .typeText(Selector('input[name="borrower_c_early_date_operation"]'),'2015-04-16')
        .wait(100)
        .click(Selector('#borrower_c_phone'))
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async profilPemohon(){
        await testController
        .typeText(Selector('input[name="borrower_pic_name"]'),'Pemohon One')
        .click(jabatanPICSelect)
        .click(jabatanPICOption.withText('Direktur Utama'))
        .wait(100)
        .typeText(Selector('#borrower_pic_ktp'),'5678912345230123')
        .setFilesToUpload('#borrower_pic_ktp_file', [
            __dirname+"/images/4.png"
        ])
        .wait(200)
        .typeText(Selector('#borrower_pic_tax_id'),'555556666677777')
        .setFilesToUpload('#borrower_pic_tax_id_file', [
            __dirname+"/images/5.png"
        ])
        .wait(200)
        .typeText(Selector('#borrower_pic_mailing_address'),'Jalan Merangin Selatan No 2')
        .wait(100)
        .click(picprovinsiSelect)
        .click(picprovinsiOption.withText('Bali'))
        .wait(100)
        .click(pickotaSelect)
        .click(pickotaOption.withText('Buleleng'))
        .wait(100)
        .click(pickecamatanSelect)
        .click(pickecamanatanOption.withText('Sukasada'))
        .wait(100)
        .click(pickelurahanSelect)
        .click(pickelurahanOption.withText('Pegadungan'))
        .wait(100)
        .typeText(Selector('#borrower_pic_phone_number'),'082118345299')
        .wait(100)
        .typeText(Selector('#borrower_pic_email'),'ikhlas.firlana+eElsC@pintek.id')
        .wait(100)
        .typeText(Selector('#borrower_pic_dob'),'1988-04-12')
        .click(Selector('input[name="borrower_pic_name"]'))
        .setFilesToUpload('#selfie_file_add', [
            __dirname+"/images/6.png"
        ])
        .wait(200)
        .typeText(Selector('#borrower_pic_spouse_name'),'Pasangan Pemohon Satu')
        .typeText(Selector('#borrower_pic_spouse_ktp'),'4444555512345678')
        .setFilesToUpload('#borrower_pic_spouse_ktp_file', [
            __dirname+"/images/7.png"
        ])
        .wait(200)
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async daftarPemilik(){
        //await debug
        await testController
        .typeText(Selector('#owner_name_add'),'Director Lee')
        .click(jabatanPemilikSelect)
        .click(jabatanPemilikOption.withText('Direktur Utama'))
        .wait(100)
        .typeText(Selector('#owner_ktp_add'),'1234912345230123')
        .setFilesToUpload('#owner_kt_file_add', [
            __dirname+"/images/8.png"
        ])
        .wait(200)
        .typeText(Selector('#owner_tax_id_add'),'855556666677779')
        .setFilesToUpload('#owner_tax_id_file_add', [
            __dirname+"/images/9.png"
        ])
        .wait(200)
        .typeText(Selector('#owner_mother_name_add'),'Ibu Direktur Lea')
        .wait(100)
        .click(Selector('#owner_name_add'))
        .click(Selector('#btn_add_owner'))
        //.click(Selector('.btn.btn-md.btn-success').withText('Simpan Sementara'))
        .takeScreenshot({
            path: 'before-click-selanjutnya.png',
            fullPage: true
        })
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'), {speed: 0.01})
        .wait(2000)
        .takeScreenshot({
            path: 'after-click-selanjutnya.png',
            fullPage: true
        })
        .wait(500000)
    },

    async daftarManajemen(){
        await testController
        .typeText(Selector('#management_name_add'),'Handi Purnomo')
        .click(jabatanManajemenSelect)
        .click(jabatanManajemenOption.withText('Direktur'))
        .wait(100)
        .typeText(Selector('#management_ktp_add'),'56124912345230123')
        .setFilesToUpload('#management_ktp_file_add', [
            __dirname+"/images/10.png"
        ])
        .wait(200)
        .typeText(Selector('#management_tax_id_add'),'255556666677771')
        .setFilesToUpload('#management_tax_id_file_add', [
            __dirname+"/images/11.png"
        ])
        .wait(200)
        .typeText(Selector('#management_mother_name_add'),'Susan Pursanti')
        .wait(100)
        .click(Selector('#btn_add_managament'))
        .wait(200)
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async penjaminPerseorangan(){
        await testController
        .typeText(Selector('#guarantor_p_name'), 'Personal Guarantor')
        .typeText(Selector('#guarantor_p_ktp'),'56124912303230789')
        .setFilesToUpload('#guarantor_p_ktp_file', [
            __dirname+"/images/12.png"
        ])
        .wait(200)
        .typeText(Selector('#guarantor_p_tax_id'),'123456666679876')
        .setFilesToUpload('#guarantor_p_tax_id_file', [
            __dirname+"/images/13.png"
        ])
        .wait(200)
        .typeText(Selector('#guarantor_p_address'),'Jalan Merbabu No 95')
        .wait(100)
        .click(guarantorprovinsiSelect)
        .click(guarantorprovinsiOption.withText('Lampung'))
        .wait(100)
        .click(guarantorkotaSelect)
        .click(guarantorkotaOption.withText('Lampung Selatan'))
        .wait(100)
        .click(guarantorkecamatanSelect)
        .click(guarantorkecamanatanOption.withText('Jati Agung'))
        .wait(100)
        .click(guarantorkelurahanSelect)
        .click(guarantorkelurahanOption.withText('Gedung Harapan'))
        .wait(100)
        .typeText(Selector('#guarantor_p_phone'),'085678901234')
        .wait(100)
        .typeText(Selector('#guarantor_p_email'),'guarantor.sme@yopmail.com')
        .wait(100)
        .typeText(Selector('#guarantor_p_spouse_name'),'Pasangan Guarantor')
        .typeText(Selector('#guarantor_p_spouse_ktp'),'7654555512348765')
        .setFilesToUpload('#guarantor_p_spouse_ktp_file', [
            __dirname+"/images/14.png"
        ])
        .wait(200)
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async penjaminBadanHukum(){
        await testController
        .click(guarantortypeSelect)
        .click(guarantortypeOption.withText('Badan Hukum/Badan Usaha'))
        .click(jenisusahagSelect)
        .click(jenisusahagOption.withText('CV'))
        .typeText(Selector('#guarantor_c_name'), 'Company Guarantor')
        .typeText(Selector('#guarantor_c_address'),'Jalan Sukaraya No 9')
        .click(guarantor2provinsiSelect)
        .click(guarantor2provinsiOption.withText('Lampung'))
        .wait(100)
        .click(guarantor2kotaSelect)
        .click(guarantor2kotaOption.withText('Lampung Selatan'))
        .wait(100)
        .click(guarantor2kecamatanSelect)
        .click(guarantor2kecamanatanOption.withText('Jati Agung'))
        .wait(100)
        .click(guarantor2kelurahanSelect)
        .click(guarantor2kelurahanOption.withText('Gedung Harapan'))
        .wait(100)
        .typeText(Selector('#guarantor_c_phone'),'085678901234')
        .wait(100)
        .typeText(Selector('#guarantor_c_tax_id'),'guarantor.sme@yopmail.com')
        .wait(100)
        .typeText(Selector('#guarantor_p_tax_id'),'123456666679876')
        .setFilesToUpload('#guarantor_c_tax_id_file', [
            __dirname+"/images/13.png"
        ])
        .wait(200)
        .typeText(Selector('#guarantor_c_pic'), 'PIC Guarantor')
        .typeText(Selector('#guarantor_c_pic_jobtitle'), 'PIC Assistant Manager')
        .wait(100)
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async DokumenWajib(){
        await testController
        .setFilesToUpload('#siup_file', [
            __dirname+"/images/15.png"
        ])
        .setFilesToUpload('#tdp_file', [
            __dirname+"/images/16.png"
        ])
        .setFilesToUpload('#finance_file', [
            __dirname+"/images/17.png"
        ])
        .setFilesToUpload('#mutation_file', [
            __dirname+"/images/18.png"
        ])
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async DokumenPendukung(){
        await testController
        .setFilesToUpload('#comp_profile_file', [
            __dirname+"/images/19.png"
        ])
        .setFilesToUpload('#activity_file', [
            __dirname+"/images/20.png"
        ])
        .setFilesToUpload('#credit_form', [
            __dirname+"/images/21.png"
        ])
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async infoTambahan(){
        await testController
        .typeText(Selector('#company_website'), 'website.domain.co')
        .typeText(Selector('#company_social_media'), 'Company Facebook')
        .typeText(Selector('#product_service_explanation'), 'Product and Service and The Explanation')
        .selectText(Selector('#sales_per_year_fake'))
        .pressKey('delete')
        .typeText(Selector('#sales_per_year_fake'), '435000000')
        .selectText(Selector('#net_income_fake'))
        .pressKey('delete')
        .typeText(Selector('#net_income_fake'), '59800000')
        .typeText(Selector('#number_of_employees'), '4500')
        .typeText(Selector('#asset_list'), 'aset satu dan aset dua')
        .typeText(Selector('#ownership_of_asset'), 'aset bersama')
        .typeText(Selector('#owner_other_business_name'), 'Other Business')
        .click(bidangusahaSelect)
        .click(bidangusahaOption.withText('Perdagangan'))
        .typeText(Selector('#customer_name_or_segment'), 'Retailer')
        .typeText(Selector('#customer_top'), 'Transfer VA')
        .typeText(Selector('#supplier_name'), 'Supplier Baru')
        .typeText(Selector('#creditor_name'), 'Creditor Lama')
        .typeText(Selector('#supplier_top'), 'Mutasi Rekening Koran')
        .click(Selector('#btn_tab2_next').withText('Selanjutnya'))
        .wait(200)
    },

    async detailPencairan(){
        await testController
        .typeText(Selector('#beneficiary_name'), 'Benefeciary Name')
        .typeText(Selector('#bank_account_no'), '105678923401')
        .click(tipeRekSelect)
        .click(tipeRekOption.withText('Giro'))
        .click(namaBankSelect)
        .click(namaBankOption.withText('Bank Danamon Indonesia, Tbk'))
        .typeText(Selector('#branch_name'), 'Cabang Bank Ini')
        .wait(100)
    },

    async SelesaiSubmit(){
        await testController
        .click(Selector('#checkbox_one_error'))
        .click(Selector('#checkbox_two_error'))
        .click(Selector('#show-success').withText('Selesai'))
        .wait(200)
    }

}