const { When, Then } = require('cucumber');
const neow = require('../support/pages/loanstep1');

When('User input all field in step 1 which borrower same as student', async function (){
    await neow.step1.personal1();
});

When('User input all field in step 1 which borrower not same as student', async function (){
    await neow.step1.personal2();
});

Then('User can see data keluarga dan rekan', async () => {
    await testController.expect(neow.getTabKeluarga()).contains('Data Pribadi');
});

When('User input all field in step 2', async function (){
    await neow.step2.keluarga();
});

When('User input all field in step 3', async function (){
    await neow.step3.pekerjaan();
});

When('User input all field in step 4', async function (){
    await neow.step4.dokumen();
});

When('User submits the application', async function (){
    await neow.finalstep.submitall();
});