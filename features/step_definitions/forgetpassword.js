const { When, Then } = require('cucumber');
const neow = require('../support/pages/neow');


When('User can click forgot password', async () => {
    await neow.navigateToForgotPswdPage();
});

Then('User can request reset password email', async function (){
    await neow.contentForgotPswd.ResetPswd();
});

Then('User can see notification success message', async function (){
    await testController.expect(neow.getSuccessResetPswd()).contains(' Email terkirim, ada di sistem kami.');
});