const { Selector } = require('testcafe');
require('dotenv').config();


const pendidikanSelect = Selector('#borrower_education');
const pendidikanOption = pendidikanSelect.find('option[value="1"]');

const pernikahanSelect = Selector('#borrower_marital_status');
const pernikahanOption = pernikahanSelect.find('option[value="2"]');

const agamaSelect = Selector('#borrower_religion');
const agamaOption = agamaSelect.find('option[value="1"]');

const provinsiSelect = Selector('#borrower_province');
const provinsiOption = provinsiSelect.find('option[value="DKI Jakarta"]');

const kotaSelect = Selector('#borrower_district');
const kotaOption = kotaSelect.find('option[value="Jakarta Pusat"]');

const kecamatanSelect = Selector('#borrower_subdistrict');
const kecamanatanOption = kecamatanSelect.find('option[value="Kemayoran"]');

const kelurahanSelect = Selector('#borrower_village');
const kelurahanOption = kelurahanSelect.find('option[value="4835"]');

const borrowerrltn1Select = Selector('#fam_borrower_relation_1');
const borrowerrltn1Option = borrowerrltn1Select.find('option[value="2"]');

const borrowerrltn2Select = Selector('#fam_borrower_relation_2');
const borrowerrltn2Option = borrowerrltn2Select.find('option[value="5"]');

const borrowerEmpTypeSelect = Selector('#borrower_employment_type');
const borrowerEmpTypeOption = borrowerEmpTypeSelect.find('option[value="4"]');

const borrowerEmpStatSelect = Selector('#borrower_employment_status');
const borrowerEmpStatOption = borrowerEmpStatSelect.find('option[value="3"]');

const provinsiEmpSelect = Selector('#borrower_employment_province');
const provinsiEmpOption = provinsiEmpSelect.find('option[value="Jawa Barat"]');

const kotaEmpSelect = Selector('#borrower_employment_district');
const kotaEmpOption = kotaEmpSelect.find('option[value="Indramayu"]');

const kecamatanEmpSelect = Selector('#borrower_employment_subdistrict');
const kecamanatanEmpOption = kecamatanEmpSelect.find('option[value="Kedokan Bunder"]');

const kelurahanEmpSelect = Selector('#borrower_employment_village');
const kelurahanEmpOption = kelurahanEmpSelect.find('option[value="9021"]');

const spendidikanSelect = Selector('#student_education');
const spendidikanOption = spendidikanSelect.find('option[value="5"]');

const spernikahanSelect = Selector('#student_marital_status');
const spernikahanOption = spernikahanSelect.find('option[value="3"]');

const sagamaSelect = Selector('#student_religion');
const sagamaOption = sagamaSelect.find('option[value="2"]');

const sprovinsiSelect = Selector('#student_province');
const sprovinsiOption = sprovinsiSelect.find('option[value="DI Yogyakarta"]');

const skotaSelect = Selector('#student_district');
const skotaOption = skotaSelect.find('option[value="Kulon Progo"]');

const skecamatanSelect = Selector('#student_subdistrict');
const skecamanatanOption = skecamatanSelect.find('option[value="Nanggulan"]');

const skelurahanSelect = Selector('#student_village');
const skelurahanOption = skelurahanSelect.find('option[value="4389"]');

const sborrowerRelation = Selector('#student_borrower_relation');
const sbrwRltnOption = sborrowerRelation.find('option[value="1"]');

const provinsiSelect2 = Selector('#borrower_dom_province');
const provinsiOption2 = provinsiSelect2.find('option[value="Kalimantan Timur"]');

const kotaSelect2 = Selector('#borrower_dom_district');
const kotaOption2 = kotaSelect2.find('option[value="Kutai Timur"]');

const kecamatanSelect2 = Selector('#borrower_dom_subdistrict');
const kecamanatanOption2 = kecamatanSelect2.find('option[value="Kongbeng"]');

const kelurahanSelect2 = Selector('#borrower_dom_village');
const kelurahanOption2 = kelurahanSelect2.find('option[value="36548"]');

exports.step1 = {
    async personal1(){
        await testController
        .typeText(Selector('#borrower_name'),'Test Case 479 324')
        .wait(200)
        .typeText(Selector('#borrower_pob'),'Godric Hollow')
        .wait(200)
        .typeText('#borrower_dob', '1993-04-04')
        .wait(1000)
        /* .takeScreenshot({
            path: 'after-input-date.png',
            fullPage: true
        }) */
        .click(Selector('#borrower_email'))
        .click(Selector('input[name="borrower_phone_no"]'))
        .typeText(Selector('input[name="borrower_phone_no"]'), '85769800705')
        /* .takeScreenshot({
            path: 'after-click-phone.png',
            fullPage: true
        }) */
        .wait(1000)
        .typeText(Selector('#borrower_email'), 'susan.satu@yopmail.com')
        .wait(200)
        .typeText(Selector('#borrower_personal_id'), '3317140404939091')
        .wait(200)
        .typeText(Selector('#borrower_tax_id'), '456785678912345')
        .wait(200)
        .click(pendidikanSelect)
        .click(pendidikanOption.withText('SMA/SMK'))
        .wait(200)
        .click(pernikahanSelect)
        .click(pernikahanOption.withText('Lajang'))
        .wait(200)
        .typeText(Selector('#borrower_depents'), '2')
        .wait(200)
        .click(agamaSelect)
        .click(agamaOption.withText('Islam'))
        .wait(1000)
        .typeText(Selector('#borrower_address'), 'Jl. Raya Pasar Minggu No.1')
        .wait(200)
        .click(provinsiSelect)
        .click(provinsiOption.withText('DKI Jakarta'))
        .wait(200)
        .click(kotaSelect)
        .click(kotaOption.withText('Jakarta Pusat'))
        .wait(200)
        .click(kecamatanSelect)
        .click(kecamanatanOption.withText('Kemayoran'))
        .wait(200)
        .click(kelurahanSelect)
        .click(kelurahanOption.withText('Utan Panjang'))
        .wait(200)
        .click(Selector('#flag_dom_address'))
        .wait(200)
        .click(Selector('input[name="home_status"]'))
        .wait(200)
        .click(Selector('input[name="borrower_student_status"]'))
        .wait(200)
        /* .click(Selector('#flag_borrower_student_address'))
        .wait(200) */
        .click(Selector('#btn-submit-personal').withText('Simpan dan Lanjut'))
        .wait(200)
        /* .takeScreenshot({
            path: 'where-are-you-1.png',
            fullPage: true
        }) */
    },
    async personal2(){
        await testController
        .typeText(Selector('#borrower_name'),'Digisign Satu')
        .wait(200)
        .typeText(Selector('#borrower_pob'),'Nashville')
        .wait(200)
        .typeText('#borrower_dob', '1983-11-08')
        .wait(1000)
        .click(Selector('#borrower_email'))
        .click(Selector('input[name="borrower_phone_no"]'))
        .typeText(Selector('input[name="borrower_phone_no"]'), '81011112222')
        .wait(1000)
        .typeText(Selector('#borrower_email'), 'dgs1@yopmail.com')
        .wait(200)
        .typeText(Selector('#borrower_personal_id'), '1010202030304040')
        .wait(200)
        .typeText(Selector('#borrower_tax_id'), '456785678912345')
        .wait(200)
        .click(pendidikanSelect)
        .click(pendidikanOption.withText('SD'))
        .wait(200)
        .click(pernikahanSelect)
        .click(pernikahanOption.withText('Lajang'))
        .wait(200)
        .typeText(Selector('#borrower_depents'), '0')
        .wait(200)
        .click(agamaSelect)
        .click(agamaOption.withText('Islam'))
        .wait(1000)
        .typeText(Selector('#borrower_address'), 'Jl. Pasar Minggu No. 37 A')
        .wait(200)
        .click(provinsiSelect)
        .click(provinsiOption.withText('DKI Jakarta'))
        .wait(200)
        .click(kotaSelect)
        .click(kotaOption.withText('Jakarta Pusat'))
        .wait(200)
        .click(kecamatanSelect)
        .click(kecamanatanOption.withText('Kemayoran'))
        .wait(200)
        .click(kelurahanSelect)
        .click(kelurahanOption.withText('Utan Panjang'))
        .wait(200)
        //.click(Selector('#flag_dom_address'))
        .typeText(Selector('#borrower_dom_address'), 'Gedung Kertajaya Blok H')
        .wait(200)
        .click(provinsiSelect2)
        .click(provinsiOption2.withText('Kalimantan Timur'))
        .wait(200)
        .click(kotaSelect2)
        .click(kotaOption2.withText('Kutai Timur'))
        .wait(200)
        .click(kecamatanSelect2)
        .click(kecamanatanOption2.withText('Kongbeng'))
        .wait(200)
        .click(kelurahanSelect2)
        .click(kelurahanOption2.withText('Miau Baru'))
        .wait(200)
        .click(Selector('input[name="home_status"]'))
        .wait(200)
        .typeText(Selector('#student_name'), 'Fred Alistair')
        .typeText(Selector('#student_pob'),'Santa Monica')
        .wait(200)
        .typeText('#student_dob', '2003-08-14')
        .wait(1000)
        .click(Selector('#student_email'))
        .typeText(Selector('#student_phone_no'), '81385431123')
        .wait(1000)
        .typeText(Selector('#student_email'), 'windy123@yopmail.com')
        .wait(200)
        .click(spendidikanSelect)
        .click(spendidikanOption.withText('S1'))
        .wait(200)
        .click(spernikahanSelect)
        .click(spernikahanOption.withText('Cerai'))
        .wait(200)
        .click(sagamaSelect)
        .click(sagamaOption.withText('Katolik'))
        .wait(1000)
        .click(sborrowerRelation)
        .click(sbrwRltnOption.withText('Ayah/Ibu'))
        .wait(200)
        .typeText(Selector('#student_address'), 'Jl. Kebon Jeruk Raya No.18')
        .wait(200)
        .click(sprovinsiSelect)
        .click(sprovinsiOption.withText('DI Yogyakarta'))
        .wait(200)
        .click(skotaSelect)
        .click(skotaOption.withText('Kulon Progo'))
        .wait(200)
        .click(skecamatanSelect)
        .click(skecamanatanOption.withText('Nanggulan'))
        .wait(200)
        .click(skelurahanSelect)
        .click(skelurahanOption.withText('Kembang'))
        .wait(200)
        .click(Selector('#btn-submit-personal').withText('Simpan dan Lanjut'))
        .wait(200)
    },
    async keluarga(){
        await testController
        .typeText(Selector('#borrower_mother_name'),'Shawn Johnson')
        .wait(200)
        .click(borrowerrltn1Select)
        .wait(200)
        .click(borrowerrltn1Option.withText('Suami/Istri'))
        .wait(200)
        .typeText(Selector('#fam_borrower_name_1'),'Andrew John East')
        .wait(200)
        .typeText(Selector('#fam_borrower_phone_1'),'82154324234')
        .wait(200)
        /* .click(borrowerrltn2Select)
        .wait(200)
        .click(borrowerrltn2Option.withText('Kakek / Nenek'))
        .wait(200)
        .typeText(Selector('#fam_borrower_name_2'),'Nenek Ariana')
        .wait(200)
        .typeText(Selector('#fam_borrower_phone_2'),'2175463653')
        .wait(200) */
        .click(Selector('#btn-submit-family').withText('Simpan'))
        .wait(200)
    }
    
}

/* try {
    result = yield _bluebird.default.race(racingPromises);
  } catch (e) {
    if (e instanceof Error) {
      error = e;
    } else if (e) {
      error = new Error(_util.default.format(e));
    } else {
      error = new Error('Promise rejected without a reason');
    }
  } */

exports.step2 = {
    async keluarga(){
        const t = await testController;
        try {
           await t
            .typeText(Selector('#borrower_mother_name'),'Shawn Johnson')
            .wait(200)
            .click(borrowerrltn1Select)
            .wait(200)
            .click(borrowerrltn1Option.withText('Suami/Istri'))
            .wait(200)
            .typeText(Selector('#fam_borrower_name_1'),'Andrew John East')
            .wait(200)
            .typeText(Selector('#fam_borrower_phone_1'),'82154324234')
            .wait(200)
            /* .click(borrowerrltn2Select)
            .wait(200)
            .click(borrowerrltn2Option.withText('Kakek / Nenek'))
            .wait(200)
            .typeText(Selector('#fam_borrower_name_2'),'Nenek Ariana')
            .wait(200)
            .typeText(Selector('#fam_borrower_phone_2'),'2175463653')
            .wait(200) */
            .click(Selector('#btn-submit-family').withText('Simpan'))
            .wait(200)
        } catch (error) {
            await t.takeScreenshot({
                path: 'where-are-you-2.png',
                fullPage: true
            })
            throw new Error(JSON.stringify(error));
        }

    }
}

exports.step3 = {
    async pekerjaan(){
        await testController
        .click(borrowerEmpTypeSelect)
        .wait(200)
        .click(borrowerEmpTypeOption.withText('Karyawan Swasta'))
        .wait(200)
        .click(borrowerEmpStatSelect)
        .wait(200)
        .click(borrowerEmpStatOption.withText('Pekerja Lepas (Freelance)'))
        .wait(200)
        .typeText(Selector('#brw_employment_name'),'AIA Financing')
        .wait(200)
        .typeText(Selector('#borrower_emplyment_startdate'),'2018-09-13')
        .wait(200)
        .typeText(Selector('#text_employment_salary'),'25750500')
        .wait(200)
        .typeText(Selector('#borrower_employment_phone_no'),'8216454324')
        .wait(200)
        .typeText(Selector('#borrower_employment_position'),'Manager')
        .wait(200)
        .typeText(Selector('input[name="borrower_employment_address"]'),'Jalan Kebun Jeruk Raya No. 80')
        .wait(200)
        .click(provinsiEmpSelect)
        .click(provinsiEmpOption.withText('Jawa Barat'))
        .wait(200)
        .click(kotaEmpSelect)
        .click(kotaEmpOption.withText('Indramayu'))
        .wait(200)
        .click(kecamatanEmpSelect)
        .click(kecamanatanEmpOption.withText('Kedokan Bunder'))
        .wait(200)
        .click(kelurahanEmpSelect)
        .click(kelurahanEmpOption.withText('Jayawinangun'))
        .wait(200)
        .click(Selector('#btn-submit-job').withText('Simpan dan Lanjut'))
        .wait(200)
    }
}


exports.step4 = {
    async dokumen(){
        await testController
        .click(Selector('#btn_upload_file_national_id'))
        .wait(200)
 /*        console.log("Directory Loc: " + __dirname) --> Directory Loc: C:\Workspace\My Automation\pintekid-qa-automation-v2\features\support\pages
        console.log("File loc: "+__filename) --> File loc: C:\Workspace\My Automation\pintekid-qa-automation-v2\features\support\pages\loanstep1.js*/
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/ktp.jpg"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_family_id'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/2.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_selfie_with_id'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/selfie.PNG"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_tax_id'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/4.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_bank_mutation'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/5.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_student_card'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/6.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .wait(200)
        /* .click(Selector('#btn_upload_file_Doc2'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/doc5.jpg"
        ])
        .click(Selector('#btnSimpanFile'))
        .wait(200) */
    }
}

exports.finalstep = {
    async submitall(){
        await testController
        .click(Selector('input[name="confirm_input"]'))
        .wait(200)
        .click(Selector('button').withText('Simpan Semuanya'))
        .click(Selector('#btn_check_status'))
        //.navigateTo ('https://neow.pintek.id/loan/list-loan')
        .wait(300)
    }
}


exports.getTabKeluarga = function () {
    return Selector('div.text-24').with({ boundTestRun: testController }).textContent;
};