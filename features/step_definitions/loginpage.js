const { Then } = require('cucumber');
const neow = require('../support/pages/neow');

Then('User can see login page', async () => {
    await testController.expect(neow.getPageTitle()).contains('Atau daftar dengan');
});