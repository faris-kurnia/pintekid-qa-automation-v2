const fs = require('fs');
const createTestCafe = require('testcafe');
const testControllerHolder = require('../support/testControllerHolder');
const {AfterAll, setDefaultTimeout, Before, After, Status} = require('cucumber');
const errorHandling = require('../support/errorHandling');
const TIMEOUT = 5 * 60 * 1000;
//const logger = RequestLogger('https://neow.pintek.id');

let isTestCafeError = false;
let attachScreenshotToReport = null;
let cafeRunner = null;
let n = 0;

function createTestFile() {
    fs.writeFileSync('test.js',
        'import errorHandling from "./features/support/errorHandling.js";\n' +
        'import testControllerHolder from "./features/support/testControllerHolder.js";\n\n' +

        'fixture("fixture")\n' +

        'test\n' +
        '("test", testControllerHolder.capture)')
}

/* function runTest(iteration, browser, skipJS) {
    if(skipJS == undefined){
        skipJS = false;
    }
    createTestCafe('localhost', 1338 + iteration, 1339 + iteration)
        .then(function(tc) {
            cafeRunner = tc;
            const runner = tc.createRunner();
            return runner
                .src('./test.js')
                .screenshots('reports/screenshots/', true)
                .browsers(browser)
                .run({skipJsErrors: skipJS})
                .catch(function(error) {
                    console.error(error);
                });
        })
        .then(function(report) {
        });
} */

function runTest(iteration, browser) {

    createTestCafe('localhost', 1338 + iteration, 1339 + iteration)
        .then(function(tc) {
            cafeRunner = tc;
            const runner = tc.createRunner();
            //const runner = tc.createLiveModeRunner();
            //const logRecord = logger.requests[0];
            return runner
                .src('./test.js')
                .screenshots('reports/screenshots/', true)
                .browsers(browser)
                .run({skipJsErrors: true})
                .catch(function(error) {
                    console.error(error);
                    console.log()
                    /* console.log(logRecord.request.url);
                    console.log(logRecord.request.method);      
                    console.log(logRecord.response.statusCode);
                    console.log(logRecord.response.headers);
                    console.log(logRecord.response.body); */
                }).reques;
        })
        .then(function(report) {
        });
}


setDefaultTimeout(TIMEOUT);

Before(function(scenario) {
    runTest(n, this.setBrowser());
    //runTest(n, this.setBrowser(), this.setSkipJS());
    createTestFile();
    n += 2;
    return this.waitForTestController.then(function(testController) {
        console.log(`Running Scenario : ${scenario.pickle.name}`);
        return testController.maximizeWindow();
    });
});

Before("@skip", function() {
    //perform some runtime check to decide whether to skip the proceeding scenario
    return 'skipped';
});

After(function() {
    fs.unlinkSync('test.js');
    testControllerHolder.free();
});

After(async function(testCase) {
    const world = this;
    if (testCase.result.status === Status.FAILED) {
        isTestCafeError = true;
        attachScreenshotToReport = world.attachScreenshotToReport;
        errorHandling.addErrorToController();
        await errorHandling.ifErrorTakeScreenshot(testController)
    }
});

AfterAll(function() {
    let intervalId = null;

    function waitForTestCafe() {
        intervalId = setInterval(checkLastResponse, 500);
    }

    function checkLastResponse() {
        if (testController.testRun.lastDriverStatusResponse === 'test-done-confirmation') {
            cafeRunner.close();
            process.exit();
            clearInterval(intervalId);
        }
    }

    waitForTestCafe();
});

const getIsTestCafeError = function() {
    return isTestCafeError;
};

const getAttachScreenshotToReport = function(path) {
    return attachScreenshotToReport(path);
};

exports.getIsTestCafeError = getIsTestCafeError;
exports.getAttachScreenshotToReport = getAttachScreenshotToReport;
